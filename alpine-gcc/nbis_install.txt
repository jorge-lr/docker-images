# Compiling NBIS using alpine-gcc:

# unzip files
unzip nbis_v5_0_0.zip

# mkdir ./Rel_5.0.0/dst

# setup
docker run --rm -v "$PWD":/usr/src/nbis \
-w /usr/src/nbis/Rel_5.0.0 jorgelr/alpine-gcc:1.0 \
./setup.sh /usr/src/nbis/Rel_5.0.0/dst --without-X11 --64

# make config
docker run --rm -v "$PWD":/usr/src/nbis \
-w /usr/src/nbis/Rel_5.0.0 jorgelr/alpine-gcc:1.0 make config

# make it
docker run --rm -v "$PWD":/usr/src/nbis \
-w /usr/src/nbis/Rel_5.0.0 jorgelr/alpine-gcc:1.0 make it

make install
docker run --rm -v "$PWD":/usr/src/nbis \
-w /usr/src/nbis/Rel_5.0.0 jorgelr/alpine-gcc:1.0 make install LIBNBIS=no

make catalog
docker run --rm -v "$PWD":/usr/src/nbis \
-w /usr/src/nbis/Rel_5.0.0 jorgelr/alpine-gcc:1.0 make catalog
